package az.ingress.Security.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/security")
@RequiredArgsConstructor
public class SecurityController {

    @GetMapping("/public")
    public String getPublic(){
        return "Hello public";
    }

    @PostMapping("/public")
    public String postPublic(){
        return "Hello post public";
    }

    @GetMapping("/user")
    public String getUser(){
        return "Hello user";
    }
    @GetMapping("/admin")
    public String getAdmin(){
        return "Hello admin";
    }
}
