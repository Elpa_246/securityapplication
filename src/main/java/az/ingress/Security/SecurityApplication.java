package az.ingress.Security;

import az.ingress.Security.model.Authority;
import az.ingress.Security.model.User;
import az.ingress.Security.model.UserAuthority;
import az.ingress.Security.repo.UserRepo;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
@RequiredArgsConstructor
@SpringBootApplication
public class SecurityApplication implements CommandLineRunner {

	private final UserRepo userRepo;
	private final BCryptPasswordEncoder passwordEncoder;
	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {


//		User pervin =User.builder()
//				.name("Pervin")
//				.password(passwordEncoder.encode("1234"))
//				.authorities(List.of(Authority.builder()
//						.userAuthority(UserAuthority.USER)
//						.build()))
//				.build();
//
//		User elvin = User.builder()
//				.name("Elvin")
//				.password(passwordEncoder.encode("1234"))
//				.authorities(List.of(Authority.builder()
//						.userAuthority(UserAuthority.USER)
//						.build(),
//						Authority.builder()
//						.userAuthority(UserAuthority.ADMIN)
//						.build()))
//
//				.build();
//
//		userRepo.save(pervin);
//		userRepo.save(elvin);
	}
}
