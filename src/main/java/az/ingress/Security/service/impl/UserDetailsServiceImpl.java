package az.ingress.Security.service.impl;

import az.ingress.Security.model.User;
import az.ingress.Security.repo.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userDb = userRepo.findByName(username).orElseThrow(() -> new RuntimeException("User Not found"));
        return  org.springframework.security.core.userdetails.User
                .builder()
                .username(userDb.getName())
                .password(userDb.getPassword())
                .authorities(userDb.getAuthorities().stream()
                        .map(auth -> new SimpleGrantedAuthority(auth.getUserAuthority().name()))
                        .collect(Collectors.toSet()))
                .build();

    }
}
