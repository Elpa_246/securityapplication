package az.ingress.Security.model;

public enum UserAuthority {
    PUBLIC,
    USER,
    ADMIN
}
